#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>

int main()
{
    std::vector<int> vec = {-120, -100,-50, -5, 1, 10, 15, 20, 75, 150};

    std::sort(vec.begin(), vec.end(), [](const auto &lhs, const auto &rhs){
        return std::abs(lhs) < std::abs(rhs);
    });

    // 1 ��ਠ�� �뢮��
    std::for_each(vec.begin(), vec.end(), [](const auto &el){
        std::cout << el << " ";
    });

    std::cout << '\n';
    /*
    // 2 ��ਠ�� �뢮��
    for (const auto &el : vec)
       std::cout << el << " ";

    std::cout << '\n';

    // 3 ��ਠ�� �뢮��
    for (size_t i = 0; i < vec.size(); ++i)
        std::cout << vec[i] << " ";

    std::cout << '\n';*/
}