#include <iostream>
#include <vector>

using namespace std;


int main()
{
    vector <int> vec = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
    int res, max = vec[0], ind_b, ind_e;
    for(int i =0; i < vec.size()-1; i++)
    {
        res = vec[i];
        for(int j = i + 1; j < vec.size(); j++){
            res += vec[j];
            if(res > max)
            {
                max = res;
                ind_b = i;
                ind_e = j;
            }
        }
    }
    cout << ind_b << " " << ind_e << "\n";
}