#include <iostream>
#include <vector>

using namespace std;


int main()
{
    vector <int> vec;
    int in;
    cin >> in;

    while (in != -1)
    {
        if(in == - 2) return 0;
        vec.emplace(vec.cbegin(), in);
        for(int i = 0; i < vec.size(); i++) // сортировка пузырьком
            for(int j = 0; j < vec.size()-1-i; j++)
                if(vec[j] > vec[j + 1])
                {
                    int temp = vec[j];
                    vec[j] = vec[j + 1];
                    vec[j + 1] = temp;
                }  
        cin >> in;
        if(in == -1)
        {
            vec.erase(vec.cbegin()+5, vec.cend() - 1 );
            vec.pop_back();
            cout << vec[4] << "\n";
            cin >> in;
        }
    }
}